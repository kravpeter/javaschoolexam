package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile)
        // TODO: Implement the logic here
         {
            if (sourceFile == null || targetFile == null) throw new IllegalArgumentException();
            String sbr1;
            ArrayList<String> br1List = new ArrayList<String>();
            BufferedReader br1;
             try {
                 br1 = new BufferedReader(new FileReader(sourceFile.getPath()));
                 String rline = br1.readLine();
                 if (rline == null) ;
                 else {
                     br1List.add(rline);
                     while ((sbr1 = br1.readLine()) != null) {
                         br1List.add(sbr1);
                     }
                     sortList(br1List);
                     if (!targetFile.exists()) {
                         new File(targetFile.getPath());
                     }

                     BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile, true));
                     Date date = new Date();
                     writer.write(date.toString() + "\r\n");
                     for (String s : br1List) {
                         writer.write(s + "\r\n");
                     }
                     writer.close();
                 }
             } catch (FileNotFoundException e) {
                 e.printStackTrace();
             } catch (IOException e) {
                 e.printStackTrace();
             }

             return true;
        }

    public static void sortList(ArrayList<String> list)
    {
        ArrayList<String> resultList = new ArrayList<String>();
        int i = 0;
        int n = list.size();
        while (i < n)
        {
            String s = list.get(i);
            int amount = 1;
            for (int j = i + 1; j < n; j++)
            {
                if (s.equals(list.get(j))) {
                    amount++;
                    list.remove(j);
                    n--;
                    j--;
                }
            }
            list.set(i, s + "[" + amount + "]");
            i++;
        }
        resultList.add(list.get(0));
        for (int j = 1; j < list.size(); j++)
        {
            if (IsBefore(resultList.get(resultList.size() - 1), list.get(j))) resultList.add(list.get(j));
            else if (IsBefore(list.get(j), resultList.get(0))) resultList.add(0, list.get(j));
            else
            {
                for (int k = 0; k < resultList.size(); k++)
                {
                    if (IsBefore(resultList.get(k), list.get(j)) && IsBefore(list.get(j), resultList.get(k + 1)))
                    {
                        resultList.add(k + 1, list.get(j));
                        break;
                    }
                }
            }
        }
        for (int j = 0; j < list.size(); j++)
        {
            list.set(j, resultList.get(j));
        }
    }

    public static boolean IsBefore(String s1, String s2)
    {
        String s = "[0123456789] ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";
        if (s1.length() > s2.length())
        {
            for (int i = 0; i < s2.length(); i++)
            {
                if (s.indexOf(s1.charAt(i)) < s.indexOf(s2.charAt(i))) return true;
                else if (s.indexOf(s1.charAt(i)) > s.indexOf(s2.charAt(i))) return false;
            }
            return false;
        }
        else
        {
            for (int i = 0; i < s1.length(); i++)
            {
                if (s.indexOf(s1.charAt(i)) < s.indexOf(s2.charAt(i))) return true;
                else if (s.indexOf(s1.charAt(i)) > s.indexOf(s2.charAt(i))) return false;
            }
            return true;
        }
    }
}
